<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Jobs\statusJob;
use Illuminate\Support\Facades\Hash;
use jeremykenedy\LaravelLogger\App\Http\Traits\ActivityLogger;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users|max:255',
            'password' => 'required|min:6'
        ]);

        $name = $request->input("name");
        $email = $request->input("email");
        $password = $request->input("password");

        $hashPwd = Hash::make($password);

        $data = [
            "name" => $name,
            "email" => $email,
            "password" => $hashPwd,
            "status" => '0'
        ];

        $id = User::create($data)->id;

        if (isset($id)) {
            
            dispatch(new statusJob($id));

            $out = [
                "message" => "register_success",
                "code"    => 201,
            ];
            ActivityLogger::activity("Register Success");
        } else {
            $out = [
                "message" => "vailed_regiser",
                "code"   => 404,
            ];
            ActivityLogger::activity("Register Failed");
        }

        return response()->json($out, $out['code']);
    }

    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $login = User::where('email', $email)->first();

        if (!$login) {
            $out = [
                        "message" => 'Your email incorrect',
                        "code"   => 200,
                    ];
            ActivityLogger::activity("Login Failed (Email Incorrect)");

            return response()->json($out, $out['code']);
        }else{
            if (Hash::check($password, $login->password)) {

                if($login->status == 0){
                    $out = [
                        "massage" => 'Your Status is Not Yet Activited',
                        "code"   => 401,
                    ];
                    ActivityLogger::activity("Status Not Activited");
                    return response()->json($out, $out['code']);
                }

                $api_token = sha1(time());
                $create_token = User::where('id', $login->id)->update(['api_token' => $api_token]);
                if ($create_token) {
                    $out = [
                        "message" => $login,
                        "api_token" => $api_token,
                        "code"   => 200,
                    ];
                    ActivityLogger::activity("Login Success");
                    return response()->json($out, $out['code']);
                }
            }else{
                $out = [
                        "message" => 'Your password incorrect',
                        "code"   => 200,
                    ];
                ActivityLogger::activity("Login Failed (Password Incorrect)");
                return response()->json($out, $out['code']);
            }
        }
    }
}


