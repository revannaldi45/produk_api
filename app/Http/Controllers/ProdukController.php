<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Produk;
use App\Jobs\CreateJob;
// use jeremykenedy\LaravelLogger\App\Http\Traits\ActivityLogger;

class ProdukController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware("auth");
    }

    public function index(){
        $data = Produk::all();
        // ActivityLogger::activity("Access Index");
        return response($data);
    }
    public function show($id){
        $data = Produk::where('id',$id)->get();
        // ActivityLogger::activity("Access Show");
        return response ($data);
    }
    public function store (Request $request){

        dispatch(new CreateJob($request->all()));
        // $data = new Produk();
        // $data->nama = $request->input('nama');
        // $data->save();
        // ActivityLogger::activity("Input Data");

        return response('Berhasil Tambah Data');
    }

    //
}
