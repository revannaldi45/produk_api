<?php

namespace App\Jobs;
use App\User;

class StatusJob extends Job
{
    private $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = User::find($this->id);
        $data->status = '1';
        $data->save();
    }
}
