<?php

namespace App\Jobs;
use App\Produk;
class CreateJob extends Job
{
    private $request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $request = $this->request;
        $data = new Produk();
        $data->nama = $request['nama'];
        $data->save();
    }
}
